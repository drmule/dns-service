FROM golang:1.18-alpine

RUN apk update && apk add git
WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

RUN go mod tidy
COPY . .

RUN go mod tidy
RUN go build -o dns-service

EXPOSE 3000

CMD [ "./dns-service"]
package handlers_test

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	log "github.com/go-kit/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/drmule/dns-service/handlers"
	calculatorsvcv1 "gitlab.com/drmule/dns-service/service/calculatorSvcV1"
	"gitlab.com/drmule/dns-service/types"
)

type testCase struct {
	Name             string `json:"name"`
	Request          string `json:"request"`
	ExpectedResponse string `json:"response"`
	ErrorExpected    bool   `json:"errorExpected"`
	StatusCode       int    `json:"statusCode"`
}

func TestGetLocation(t *testing.T) {

	// setup handler
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	locSvc := calculatorsvcv1.New(logger)
	locationHandler := handlers.New(locSvc, logger)

	//read test data
	var tests []testCase

	os.Setenv(types.SectorIDEnv, "21")

	data, err := os.ReadFile("testdata/getLocation.json")
	if err != nil {
		t.Errorf("error while reading testdata json file: %v", err)
		return
	}

	err = json.Unmarshal(data, &tests)
	if err != nil {
		t.Errorf("error while parsing testdata json file: %v", err)
		return
	}

	for _, tc := range tests {
		req, _ := http.NewRequest(http.MethodPost, "/api/v1/location", strings.NewReader(tc.Request))
		w := httptest.NewRecorder()

		locationHandler.GetLocation(w, req)

		res := w.Result()
		defer res.Body.Close()
		data, err := ioutil.ReadAll(res.Body)
		assert.Equal(t, tc.StatusCode, res.StatusCode, tc.Name+"wrong status code got")
		if err != nil {
			t.Errorf("internal error: %v", err)
		}
		assert.Equal(t, tc.ExpectedResponse, string(data), tc.Name+"expected output mismatch")

	}

}

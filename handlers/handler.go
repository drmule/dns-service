package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"

	log "github.com/go-kit/log"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"gitlab.com/drmule/dns-service/pkg/location"
	"gitlab.com/drmule/dns-service/service"
	"gitlab.com/drmule/dns-service/types"
)

type Handler struct {
	locSvc service.LocationSvc
	logger log.Logger
}

func New(locSvc service.LocationSvc, logger log.Logger) *Handler {
	return &Handler{locSvc: locSvc, logger: logger}
}

func (h *Handler) RegisterHandler(r *mux.Router) {
	r.HandleFunc(types.LocationV1Path, h.GetLocation).Methods(http.MethodPost)
}

// GetLocation handles api/v1/location post request
func (h *Handler) GetLocation(w http.ResponseWriter, r *http.Request) {

	//r.Header.Get(auth.A)

	// get request id
	reqID := r.Header.Get(types.RequestIDHeader)
	h.logger.Log(types.Info, fmt.Sprintf("[%s]: Get location api started...", reqID))

	var locReq location.LocationRequest
	// Get request and validate request
	err := json.NewDecoder(r.Body).Decode(&locReq)
	if err != nil {
		h.logger.Log(types.Error, fmt.Sprintf("[%s] bad request error: %v", reqID, err))
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errors.Wrapf(err, "bad request error:").Error()))
		return
	}

	// sector id preference--
	// 1. from request header/client
	// 2. env variable
	// 3. default
	sectorID := r.Header.Get(types.SectorIDHeader)
	if sectorID == "" {
		sectorID = os.Getenv(types.SectorIDEnv)
		if sectorID == "" {
			sectorID = types.DefaultSectorId
		}
	}

	/*err = validateRequest(locReq)
	if err != nil {
		fmt.Printf("\n validation error: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errors.Wrapf(err, "validation error:").Error()))
		return
	}
	*/
	sectorIdFloat, err := strconv.ParseFloat(sectorID, 64)
	if err != nil {
		h.logger.Log(types.Error, fmt.Sprintf("[%s] bad request error:header sector-id should be numeric", reqID))
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("bad request error:header sector-id should be numeric"))
		return

	}
	res, code, err := h.locSvc.CalculateLocation(locReq, reqID, sectorIdFloat)
	if err != nil {
		h.logger.Log(types.Error, fmt.Sprintf("[%s]: error while calculating location: error: %v, code: %d", reqID, err, code))
		w.WriteHeader(code)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(res)
	h.logger.Log(types.Info, fmt.Sprintf("[%s]: Get location api completed...", reqID))
}

/*
func validateRequest(locReq location.LocationRequest) error {

	_, err := strconv.ParseFloat(locReq.XCord, 64)
	if err != nil {
		return errors.Wrapf(err, "value of 'x' is not valid float value")
	}

	_, err = strconv.ParseFloat(locReq.YCord, 64)
	if err != nil {
		return errors.Wrapf(err, "value of 'x' is not valid float value")
	}
	_, err = strconv.ParseFloat(locReq.ZCord, 64)
	if err != nil {
		return errors.Wrapf(err, "value of 'x' is not valid float value")
	}

	_, err = strconv.ParseFloat(locReq.Velocity, 64)
	if err != nil {
		return errors.Wrapf(err, "value of 'vel' is not valid float value")
	}
	return nil
}
*/

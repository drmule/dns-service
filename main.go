package main

import (
	"io"
	stdlog "log"
	"net/http"
	"os"

	log "github.com/go-kit/log"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/drmule/dns-service/handlers"
	"gitlab.com/drmule/dns-service/middleware"
	calculatorsvcv1 "gitlab.com/drmule/dns-service/service/calculatorSvcV1"
	"gitlab.com/drmule/dns-service/types"
)

func main() {

	r := mux.NewRouter()

	// adding middleware for metrics

	// logging

	logger := log.NewLogfmtLogger(log.StdlibWriter{})
	// Direct any attempts to use Go's log package to our structured logger
	stdlog.SetOutput(log.NewStdlibAdapter(logger, log.FileKey("logs")))
	// log structure
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "loc", log.DefaultCaller)
	// Prometheus endpoint

	f, err := os.OpenFile("service_log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		stdlog.Fatalf("error opening file: %v", err)
	}
	defer f.Close()

	wrt := io.MultiWriter(os.Stdout, f)
	stdlog.SetOutput(wrt)

	im := middleware.New(logger)
	r.Use(im.PrometheusMiddleware)
	r.Path("/prometheus").Handler(promhttp.Handler())

	//r.Use()

	locSvc := calculatorsvcv1.New(logger)
	locationHandler := handlers.New(locSvc, logger)
	locationHandler.RegisterHandler(r)

	logger.Log(types.Info, "setting up server....")
	port := os.Getenv(types.PortEnv)
	if port == "" {
		port = types.DefaultPort
	}
	http.ListenAndServe(":"+port, r)
}

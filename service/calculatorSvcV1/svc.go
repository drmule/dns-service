package calculatorsvcv1

import (
	"errors"
	"fmt"
	"math"
	"net/http"

	log "github.com/go-kit/log"
	"gitlab.com/drmule/dns-service/pkg/location"
	"gitlab.com/drmule/dns-service/service"
	"gitlab.com/drmule/dns-service/types"
)

const (
	SectorID = 1 //todo read from config/env vars
)

type Svc struct {
	logger log.Logger
}

func New(logger log.Logger) *Svc {
	return &Svc{logger: logger}
}

func (s *Svc) CalculateLocation(req location.LocationRequest, reqID string, sectorID float64) (*location.LocationResponse, int, error) {

	s.logger.Log(types.Info, fmt.Sprintf("[%s]: parsing location request ", reqID))
	response := &location.LocationResponse{}
	reqVals, err := service.ParseLocRequest(req)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	if len(reqVals) != 4 {
		return nil, http.StatusBadRequest, errors.New("validation error: bad request")
	}
	s.logger.Log(types.Info, fmt.Sprintf("[%s]: calculating location according to v1 formula", reqID))
	// applying formula and rounding upto 2 decimal places
	loc := sectorID*reqVals[0] + sectorID*reqVals[1] + sectorID*reqVals[2] + reqVals[3]
	response.Location = math.Round(loc*100) / 100
	s.logger.Log(types.Info, fmt.Sprintf("[%s]: location is calculated...", reqID))
	return response, http.StatusOK, nil
}

package service

import (
	"strconv"

	"github.com/pkg/errors"
	"gitlab.com/drmule/dns-service/pkg/location"
)

type LocationSvc interface {
	CalculateLocation(req location.LocationRequest, reqID string, sectorID float64) (*location.LocationResponse, int, error)
}

func New() {
	//return locationSvc
}

// ParseLocRequest parse cordinates and velocity to float64 and throws error if needed
func ParseLocRequest(locReq location.LocationRequest) ([]float64, error) {

	requestValues := make([]float64, 0)
	x, err := strconv.ParseFloat(locReq.XCord, 64)
	if err != nil {
		return requestValues, errors.Wrapf(err, "value of 'x' is not valid float value")
	}

	requestValues = append(requestValues, x)

	y, err := strconv.ParseFloat(locReq.YCord, 64)
	if err != nil {
		return requestValues, errors.Wrapf(err, "value of 'y' is not valid float value")
	}
	requestValues = append(requestValues, y)
	z, err := strconv.ParseFloat(locReq.ZCord, 64)
	if err != nil {
		return requestValues, errors.Wrapf(err, "value of 'z' is not valid float value")
	}
	requestValues = append(requestValues, z)

	vel, err := strconv.ParseFloat(locReq.Velocity, 64)
	if err != nil {
		return requestValues, errors.Wrapf(err, "value of 'vel' is not valid float value")
	}
	requestValues = append(requestValues, vel)
	return requestValues, nil
}

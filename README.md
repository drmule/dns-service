# DNS service

## Problem Statement

2218 Space exploration is underway and mostly done by private companies. You joined
Engineering department of one of the private government contractors, Atlas Corporation.
In that year and age, everything is automated, so survey and data gathering is done by drones.
Unfortunately, although drones are perfectly capable of gathering the data, they have issues
with locating databank to upload gathered data. You, as the most promising recruit of Atlas
Corp, were tasked to develop a drone navigation service (DNS):
1. each observed sector of the galaxy has unique numeric SectorID assigned to it
2. each sector will have at least one DNS deployed
3. each sector has different number of drones deployed at any given moment
4. it’s future, but not that far, so drones will still use JSON REST API
DNS should be designed with following constraints in mind:
5. SectorID can be considered constant (it won’t change during runtime)
6. DNS request example:<br>
<code>
{
// x, y, z are coords, vel is velocity
// (values sent as strings, but must be treated as floating point number)<br>
"x": "123.12",<br>
"y": "456.56",<br>
"z": "789.89",<br>
"vel": "20.0",<br>
}
</code>

## Setup
### local
1. Install Go programming language: https://go.dev/doc/install
2. Run go mod tidy
3. Run go build 
4. Run ./dns-service
5. Service will be started on http://localhost:3000
6. If you want to change port and sectorId os env vars

### docker
1. Clone this repo: https://gitlab.com/drmule/dns-service.git
2. cd dns-service
3. docker build --tag dns-service .
4. check image by **docker ps ** command
5. Run the image
   docker run --publish 3000:3000 dns-service

### Postman collecting
You can try postman collecting to get started with sample requests:<br>
https://gitlab.com/drmule/dns-service/-/blob/main/docs/DNS.postman_collection.json

package middleware

import (
	"fmt"
	"net/http"
	"strconv"

	log "github.com/go-kit/log"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/urfave/negroni"
	"gitlab.com/drmule/dns-service/types"
)

type intrumentedMiddleware struct {
	logger log.Logger
}

var requestCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Number of requests.",
	},
	[]string{"path"},
)

var statusCodeCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "response_status",
		Help: "Status of HTTP response",
	},
	[]string{"status"},
)

var responseTime = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name: "http_response_time_seconds",
	Help: "Duration of HTTP requests.",
}, []string{"path"})

func New(logger log.Logger) *intrumentedMiddleware {
	prometheus.Register(requestCounter)
	prometheus.Register(statusCodeCounter)
	prometheus.Register(responseTime)
	return &intrumentedMiddleware{logger: logger}
}

// PrometheusMiddleware is a middleware handler used to inject logs and published metrics to prometheus
func (im *intrumentedMiddleware) PrometheusMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		route := mux.CurrentRoute(r)
		path, _ := route.GetPathTemplate()

		timer := prometheus.NewTimer(responseTime.WithLabelValues(path))
		//rw := http.NewResponseWriter(w)

		lrw := negroni.NewResponseWriter(w)
		// trxID is unique id to recored in logs for each request
		reqID := r.Header.Get(types.RequestIDHeader)
		if reqID == "" {
			reqID = uuid.New().String()
			r.Header.Set(types.RequestIDHeader, reqID)
		}
		im.logger.Log(types.Info, fmt.Sprintf("[%s]: %s request is serving....", reqID, path))
		next.ServeHTTP(lrw, r)

		statusCode := lrw.Status()
		//statusCode := w.Header().Get()
		//w.WriteHeader()

		w.Header().Add(types.RequestIDHeader, reqID)
		statusCodeCounter.WithLabelValues(strconv.Itoa(statusCode)).Inc()
		requestCounter.WithLabelValues(path).Inc()

		timer.ObserveDuration()
		im.logger.Log(types.Info, fmt.Sprintf("[%s]: %s request is completed with status code %d....", reqID, path, statusCode))
	})
}

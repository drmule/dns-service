package types

const (
	// paths
	LocationV1Path = "/api/v1/location"

	// Headers
	RequestIDHeader = "REQUEST-ID"
	SectorIDHeader  = "SECTOR-ID"

	// Log level
	Error = "ERROR"
	Info  = "INFO"
	Warn  = "WARNING"

	// env var
	PortEnv     = "port"
	SectorIDEnv = "sectorId"

	// default values
	DefaultPort     = "3000"
	DefaultSectorId = "1"
)

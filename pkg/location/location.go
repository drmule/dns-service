package location

type LocationRequest struct {
	XCord    string `json:"x"`
	YCord    string `json:"y"`
	ZCord    string `json:"z"`
	Velocity string `json:"vel"`
}

type LocationResponse struct {
	Location float64 `json:"loc"`
}
